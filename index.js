window.onload = function() {
    const wrapper = document.getElementById('wrapper');
    const base = document.createElement('div');
    const vinylRecord = document.createElement('div');
    const arrow = document.createElement('div');
    const nameSong = document.createElement('div');
    const play_pause = document.createElement('img');
    const setting = document.createElement('div');
    const button_prev = document.createElement('img');
    const button_next = document.createElement('img');
    const time = document.createElement('div');
    const playList = document.createElement('div');        

    base.classList.add('base');
    vinylRecord.classList.add('vinylRecord');

    arrow.classList.add('arrow');
    nameSong.classList.add('nameSong');
    play_pause.classList.add('play_pause');
    play_pause.classList.add('play');
    setting.classList.add('setting');
    button_prev.classList.add('change');
    button_next.classList.add('change');
    time.classList.add('time');
    playList.classList.add('playList');

    play_pause.setAttribute('id', -1);
    button_prev.setAttribute('id', 'prev');
    button_prev.setAttribute('data-id', -1);
    button_next.setAttribute('id', 'next');
    button_next.setAttribute('data-id', -1);

    play_pause.src = './image/button.png';
    button_prev.src = './image/prev.png';
    button_next.src = './image/next.png';

    setting.append(button_prev, button_next);
    wrapper.append(base, vinylRecord, arrow, nameSong, play_pause, setting, time, playList);

    let id_song = -1;
    let curTime;
    let Song;
    const songs = [
        muz_one = [0, 'DJ Combo - Better off alone', 'better_off_alone.mp3', '32.496325'],
        muz_two = [1, 'Tones And I - Dance monkey', 'dance_monkey.mp3', '26.01795'],
        muz_three = [2, 'Saint John - Roses (Remix)', 'roses.mp3', '30.19755']
    ];
/*
// Получила продолжительность песни//
    Song = new Audio(songs[1][2]);
    Song.addEventListener('loadedmetadata', function(){
        console.log(this.duration);
    });
*/

//Запуск нового трека//
    const playNewSong = (id) => {
        nameSong.innerHTML = songs[id][1];
        const circleType = new CircleType(nameSong);
        circleType.radius(80).dir(2);
        play_pause.setAttribute('id', id);
        id_song = id;
        Song = new Audio(songs[id][2]);
        Song.play();
        Song.addEventListener('timeupdate', () => {
            curTime = Song.currentTime;
            time.innerHTML = parseInt(curTime/60)+':'+parseInt(curTime%60);
        });
        vinylRecord.classList.add('move');
        arrow.classList.add('arrow_play');

        Song.addEventListener('ended', () => {
            vinylRecord.classList.remove('move');
            arrow.classList.remove('arrow_play');
        });
    };

    // console.log(`Загрузка началась ${performance.now()}мс назад`);

//Проверка запущенного трэка checkTrack()//
    const playPauseSong = (id) => {
        if(Song) {
            if(id == id_song) {
                if(Song.paused) {
                    Song.play();
                    vinylRecord.classList.add('move');
                    arrow.classList.add('arrow_play');
                } else{
                    Song.pause();
                    vinylRecord.classList.remove('move');
                    arrow.classList.remove('arrow_play');
                }
            } else{
                Song.pause();
                vinylRecord.classList.remove('move');
                arrow.classList.remove('arrow_play');
                playNewSong(id);
            }
        } else{
            playNewSong(id);
        }
    };

// Создаем список песен//
    for(let i = 0; i < songs.length; i++) {
        const song = document.createElement('div');
        const nameSong_song = document.createElement('div');
        const duration_song = document.createElement('div');

        song.classList.add('song');
        song.classList.add('play');
        nameSong_song.classList.add('nameSong_song');
        duration_song.classList.add('duration_song');
        
        song.id = songs[i][0];
        nameSong_song.innerHTML = songs[i][1];
        duration_song.innerHTML = parseInt(songs[i][3]/60)+':'+parseInt(songs[i][3]%60);
        song.append(nameSong_song, duration_song);
        playList.append(song);
    };

    const allPlay = document.getElementsByClassName('play');

//Запуск/пауза по нажатию на трек или кнопку//
    for(let i = 0; i < allPlay.length; i++) {
        allPlay[i].addEventListener('click', function() {

            let id = this.getAttribute('id');
            playPauseSong(id);
            id++;
            button_next.setAttribute('data-id', id);
            id--;id--;
            button_prev.setAttribute('data-id', id);
        });
    };
    
    let changeTrack = document.getElementsByClassName('change');

    for(let i = 0; i < changeTrack.length; i++) {
        changeTrack[i].addEventListener('click', function() {
            let id = this.getAttribute('data-id');
            if(id != -1) {
                playPauseSong(id);
                id++;
                button_next.setAttribute('data-id', id);  
                id--;id--;
                button_prev.setAttribute('data-id', id);      
            }
        });
    };    
};